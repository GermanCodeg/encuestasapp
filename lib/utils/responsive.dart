// flutter
import 'package:flutter/material.dart';

class Re {
  static double screenWidth(BuildContext context) =>
      MediaQuery.of(context).size.width;
  static double screenHeight(BuildContext context) =>
      MediaQuery.of(context).size.height;

  static double widthRe({
    required BuildContext context,
    required double width,
  }) =>
      screenWidth(context) * (width / 100);

  static double heightRe({
    required double heigh,
    required BuildContext context,
  }) =>
      screenHeight(context) * (heigh / 100);

  static double adaptiveTextSize(double value, BuildContext context) =>
      (value / 720) * MediaQuery.of(context).size.height;

  static Widget textRe({
    required double textWidth,
    required Text text,
  }) {
    return SizedBox(
      width: textWidth,
      child: FittedBox(
        fit: BoxFit.fitWidth,
        child: text,
      ),
    );
  }
}
