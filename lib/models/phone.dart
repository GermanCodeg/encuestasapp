class Phone {
  int number;

  Phone({
    required this.number,
  });

  Map<String, dynamic> toJson() => {
        "number": number,
      };

  factory Phone.fromJson(Map<String, dynamic> json) => Phone(
        number: json["number"],
      );
}

final phones = [
  Phone(
    number: 40841293888,
  ).toJson(),
  Phone(
    number: 7501035911567,
  ).toJson(),
];
