// flutter
import 'package:flutter/cupertino.dart';

// pages
import 'package:app_encuestas/pages/pages.dart';

Map<String, WidgetBuilder> getRoutes() {
  return {
    "/": (BuildContext context) => const Login(),
    "/home": (BuildContext context) => const Home(),
    "/quiz": (BuildContext context) => const Quiz(),
  };
}
