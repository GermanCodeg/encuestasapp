// lib
import 'package:provider/provider.dart';

// flutter
import 'package:flutter/material.dart';

// const
import 'package:app_encuestas/const/const.dart';

// utils
import 'package:app_encuestas/utils/responsive.dart';

// widgets
import 'package:app_encuestas/widgets/text_field_phone_container.dart';
import 'package:app_encuestas/widgets/text_field_tfact_container.dart';
import 'package:app_encuestas/widgets/phone_not_found_dialog.dart';
import 'package:app_encuestas/widgets/intents_fail_dialog.dart';

// models
import 'package:app_encuestas/models/phone.dart';

// providers
import 'package:app_encuestas/providers/text_controller.dart';
import 'package:app_encuestas/providers/login_provider.dart';
import 'package:app_encuestas/providers/phone.dart';
import 'package:app_encuestas/providers/db.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late TextControllerProvider textControllerProvider;
  late LoginProvider loginProvider;

  @override
  void initState() {
    // TODO: implement initState
    onStart();
    super.initState();
  }

  void onStart() async {
    await DBProvider.db();
  }

  @override
  Widget build(BuildContext context) {
    textControllerProvider = Provider.of<TextControllerProvider>(context);
    textControllerProvider = Provider.of<TextControllerProvider>(context);
    loginProvider = Provider.of<LoginProvider>(context);
    return Material(
      child: Stack(
        children: [
          _bg(),
          SizedBox(
            width: Re.screenWidth(context),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                _logo(),
                loginProvider.getIsTwoFactors
                    ? const TextFieldTfactContainer()
                    : const TextFieldPhoneContainer(),
                _btnInit(),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _bg() => Image.asset(
        bgUrl,
        width: Re.screenWidth(context),
        height: Re.screenHeight(context),
        fit: BoxFit.cover,
      );

  Widget _logo() => SizedBox(
        width: Re.widthRe(
          context: context,
          width: 70,
        ),
        height: Re.heightRe(
          context: context,
          heigh: 40,
        ),
        child: Image.asset(
          logoUrl,
          fit: BoxFit.contain,
        ),
      );

  Widget _btnInit() {
    return Container(
      width: Re.widthRe(
        context: context,
        width: 50,
      ),
      padding: EdgeInsets.only(
        top: Re.heightRe(
          context: context,
          heigh: 5,
        ),
      ),
      child: FittedBox(
        fit: BoxFit.contain,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.black,
            padding: const EdgeInsets.symmetric(
              horizontal: 80,
              vertical: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          onPressed: () async {
            if (loginProvider.getIsTwoFactors) {
              ifIsTwoFactors();
            } else {
              ifNotTwoFactors();
            }
          },
          child: const Text(
            "Iniciar",
            style: TextStyle(
              color: Colors.white,
              fontSize: 30,
              fontFamily: 'san serif',
            ),
          ),
        ),
      ),
    );
  }

  void ifIsTwoFactors() async {
    String code = textControllerProvider.textTfactController.text;
    if (code == codeTwoFactors) {
      loginProvider.cleanIntentsTwoFactors();
      textControllerProvider.cleanTfactController();
      Navigator.pushNamed(context, homeRoutePage);
    } else {
      if (loginProvider.getintentsTwoFactors >= 1) {
        await IntentsFailDialog(context: context).show();
        loginProvider.cleanIntentsTwoFactors();
        loginProvider.sendIsTwoFactors(false);
        textControllerProvider.cleanTfactController();
      }
      loginProvider.sumIntentsTwoFactors();
    }
  }

  void ifNotTwoFactors() async {
    String number = textControllerProvider.textPhoneController.text;
    Phone? phone = await PhoneProvider.getPhone(int.parse(number));
    if (phone != null) {
      loginProvider.sendIsTwoFactors(true);
      textControllerProvider.cleanTextPhoneController();
    } else {
      await PhoneNotFoundDialog(context: context).show();
      textControllerProvider.cleanTextPhoneController();
    }
  }
}
