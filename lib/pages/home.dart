// flutter
import 'package:app_encuestas/widgets/button_in_area.dart';
import 'package:flutter/material.dart';

// utils
import 'package:app_encuestas/utils/responsive.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: Container(
          width: Re.screenWidth(context),
          height: Re.screenHeight(context),
          color: const Color(0xffF7F8FE),
          child: Column(
            children: [
              _header(),
              _section1(),
              _section2(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _header() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Re.textRe(
                text: const Text(
                  "Hola",
                  style: TextStyle(
                    color: Color(0xff25374A),
                  ),
                ),
                textWidth: 30,
              ),
              Re.textRe(
                text: const Text(
                  "Valentina",
                  style: TextStyle(
                    color: Color(0xff25374A),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                textWidth: 70,
              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Icon(
                Icons.card_giftcard_outlined,
                color: const Color(0xff25374A),
                size: Re.widthRe(context: context, width: 5),
              ),
              Re.textRe(
                text: const Text(
                  "705",
                  style: TextStyle(
                    color: Color(0xff25374A),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                textWidth: 40,
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _section1() {
    return Padding(
      padding: const EdgeInsets.only(top: 18.0),
      child: Container(
        width: Re.widthRe(
          context: context,
          width: 90,
        ),
        height: Re.heightRe(
          heigh: 30,
          context: context,
        ),
        decoration: const BoxDecoration(
          color: Color(0xff722C73),
          borderRadius: BorderRadius.all(
            Radius.circular(15),
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Re.textRe(
              text: const Text(
                "Encuestas disponibles",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              textWidth: 200,
            ),
            const SizedBox(
              height: 10,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  primary: Colors.white,
                  padding: const EdgeInsets.symmetric(horizontal: 35.0)),
              onPressed: () {
                Navigator.pushNamed(context, "/quiz");
              },
              child: Re.textRe(
                text: const Text(
                  "Ir ahora",
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                textWidth: 70,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _section2() {
    return Container(
      width: Re.widthRe(
        context: context,
        width: 90,
      ),
      padding: const EdgeInsets.only(top: 30.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Re.textRe(
            text: const Text(
              "Próximas encuestas",
              style: TextStyle(
                color: Color(0xff25374A),
                fontWeight: FontWeight.bold,
              ),
            ),
            textWidth: 170,
          ),
          const SizedBox(
            height: 10.0,
          ),
          SizedBox(
            height: Re.heightRe(
              heigh: 35,
              context: context,
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    width: Re.widthRe(
                      context: context,
                      width: 90,
                    ),
                    height: Re.heightRe(
                      heigh: 20,
                      context: context,
                    ),
                    decoration: const BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                      gradient: LinearGradient(
                        colors: [Color(0xffDC5353), Colors.black],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 15,
                        bottom: 15,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Re.textRe(
                            text: const Text(
                              "Mascara mega full curve",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            textWidth: 200,
                          ),
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 15.0),
                  Container(
                    width: Re.widthRe(
                      context: context,
                      width: 90,
                    ),
                    height: Re.heightRe(
                      heigh: 20,
                      context: context,
                    ),
                    decoration: const BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                      gradient: LinearGradient(
                        colors: [Color(0xffFFBCDC), Colors.black],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(
                        left: 15,
                        bottom: 15,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Re.textRe(
                            text: const Text(
                              "Empoderando mujeres",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            textWidth: 200,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
