// flutter
import 'package:flutter/material.dart';

// utils
import 'package:app_encuestas/utils/responsive.dart';

class Quiz extends StatefulWidget {
  const Quiz({Key? key}) : super(key: key);

  @override
  _QuizState createState() => _QuizState();
}

class _QuizState extends State<Quiz> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: SizedBox(
          height: Re.screenHeight(context),
          width: Re.screenWidth(context),
          child: Column(
            children: [
              _toBackBtn(),
              _btnLabialMate(),
              const SizedBox(
                height: 15,
              ),
              _camera(),
              const SizedBox(
                height: 15,
              ),
              _info(),
              const SizedBox(
                height: 15,
              ),
              _points(),
              _btnInitQuiz(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _toBackBtn() {
    return SizedBox(
      width: Re.screenWidth(context),
      child: ElevatedButton(
        onPressed: () {
          Navigator.pushNamed(context, "/home");
        },
        style: ElevatedButton.styleFrom(
          primary: Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(
              Icons.arrow_back,
              size: Re.widthRe(
                context: context,
                width: 10,
              ),
              color: Colors.black,
            )
          ],
        ),
      ),
    );
  }

  Widget _camera() {
    return GestureDetector(
      onTap: () {},
      child: Image.asset(
        "assets/quiz/img/open_camera.png",
        width: Re.widthRe(
          context: context,
          width: 80,
        ),
        height: Re.heightRe(
          heigh: 40,
          context: context,
        ),
        fit: BoxFit.contain,
      ),
    );
  }

  Widget _info() {
    return Container(
      width: Re.widthRe(
        context: context,
        width: 85,
      ),
      height: Re.heightRe(
        heigh: 15,
        context: context,
      ),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.yellow, width: 2),
        borderRadius: const BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Re.textRe(
          textWidth: 10,
          text: const Text(
            "Este test tiene como objetivo, evaluar\n"
            "como se comporta el producto sobre\n"
            "la tez, de mujeres latinas.",
            style: TextStyle(
              color: Colors.black,
            ),
          ),
        ),
      ),
    );
  }

  Widget _points() {
    return SizedBox(
      height: Re.heightRe(
        heigh: 8,
        context: context,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Re.textRe(
            textWidth: 80,
            text: const Text("Este test otorga"),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.card_giftcard_sharp,
                size: Re.widthRe(
                  context: context,
                  width: 10,
                ),
              ),
              Re.textRe(
                textWidth: 60,
                text: const Text(
                  "34 puntos",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget _btnInitQuiz() {
    return Container(
      width: Re.widthRe(
        context: context,
        width: 50,
      ),
      padding: EdgeInsets.only(
        top: Re.heightRe(
          context: context,
          heigh: 1,
        ),
      ),
      child: FittedBox(
        fit: BoxFit.contain,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.black,
            padding: const EdgeInsets.symmetric(
              horizontal: 80,
              vertical: 20,
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          onPressed: () {},
          child: const Text(
            "Iniciar Test",
            style: TextStyle(
              color: Colors.white,
              fontSize: 30,
              fontFamily: 'san serif',
            ),
          ),
        ),
      ),
    );
  }

  Widget _btnLabialMate() {
    return Container(
      width: Re.widthRe(
        context: context,
        width: 62,
      ),
      height: Re.heightRe(
        heigh: 10,
        context: context,
      ),
      decoration: const BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
        gradient: LinearGradient(
          colors: [Color(0xffE02732), Color(0xffD52953), Color(0xff8B1927)],
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Re.textRe(
              text: const Text(
                "Labial Hidracolor Mate",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
              textWidth: 200,
            ),
          ],
        ),
      ),
    );
  }
}
