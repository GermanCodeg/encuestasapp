// flutter
import 'package:flutter/material.dart';

// models
import 'package:app_encuestas/models/phone.dart';

// providers
import 'package:app_encuestas/providers/db.dart';

class PhoneProvider {
  static Future<Phone?> getPhone(int number) async {
    final db = await DBProvider.db();

    final res = await db.query(
      "phone",
      where: "number = ?",
      whereArgs: [number],
      limit: 1,
    );

    return res.isNotEmpty ? Phone.fromJson(res.first) : null;
  }
}
