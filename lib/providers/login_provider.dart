// flutter
import 'package:flutter/material.dart';

class LoginProvider extends ChangeNotifier {
  bool isTwoFactors = false;
  int intentsTwoFactors = 0;

  void sendIsTwoFactors(bool val) {
    isTwoFactors = val;
    notifyListeners();
  }

  bool get getIsTwoFactors => isTwoFactors;

  void sumIntentsTwoFactors() {
    if (intentsTwoFactors < 2) {
      intentsTwoFactors++;
      notifyListeners();
    }
  }

  void cleanIntentsTwoFactors() {
    intentsTwoFactors = 0;
    notifyListeners();
  }

  int get getintentsTwoFactors => intentsTwoFactors;
}
