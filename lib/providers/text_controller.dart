// flutter
import 'package:flutter/material.dart';

class TextControllerProvider extends ChangeNotifier {
  late TextEditingController textPhoneController = TextEditingController();
  late TextEditingController textTfactController = TextEditingController();

  void cleanTextPhoneController() {
    textPhoneController.text = '';
    notifyListeners();
  }

  void cleanTfactController() {
    textTfactController.text = '';
    notifyListeners();
  }
}
