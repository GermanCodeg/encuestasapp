// lib
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart';
import 'dart:io';

// models
import 'package:app_encuestas/models/phone.dart';

class DBProvider {
  static Future<void> createTables(sql.Database database) async {
    await database.execute(
      """
      CREATE TABLE phone(
        number INTEGER
      )
      """,
    );
  }

  static Future<sql.Database> db() async {
    Directory documentsDirectory =
        await getApplicationDocumentsDirectory(); //tener el path de nuestra BD

    final path = join(
      documentsDirectory.path,
      'app.db',
    ); //el path completo con el nombre de la db

    return sql.openDatabase(
      path,
      version: 1,
      onCreate: (sql.Database db, int version) async {
        await createTables(db);
        await createPhones(db);
      },
      onOpen: (sql.Database db) async {},
    );
  }

  static Future<void> createPhones(sql.Database db) async {
    for (Map<String, dynamic> phone in phones) {
      await db.insert('phone', phone);
    }
  }
}
