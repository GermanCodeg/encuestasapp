// assets to login page
const bgUrl = "assets/login/img/bg.png";
const logoUrl = "assets/login/img/heippi-metrics-logo-blanco.png";

// const logic
const codeTwoFactors = "0403";

// routes
const loginRoutePage = "/";
const homeRoutePage = "/home";
