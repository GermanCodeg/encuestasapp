// lib
import 'package:provider/provider.dart';

// flutter
import 'package:flutter/material.dart';

// utils
import 'package:app_encuestas/utils/responsive.dart';

// widgets
import 'package:app_encuestas/widgets/button_in_area.dart';

// providers
import 'package:app_encuestas/providers/login_provider.dart';

class IntentsFailDialog {
  BuildContext context;
  IntentsFailDialog({required this.context});
  late LoginProvider loginProvider;

  Future show() {
    loginProvider = Provider.of<LoginProvider>(context, listen: false);
    return showDialog(
      context: context,
      builder: (_) => Dialog(
        backgroundColor: Colors.transparent,
        child: SizedBox(
          width: Re.widthRe(
            context: context,
            width: 50,
          ),
          height: Re.heightRe(
            context: context,
            heigh: 40,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              _body(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _body() {
    return Container(
      width: Re.widthRe(
        context: context,
        width: 70,
      ),
      height: Re.heightRe(
        heigh: 20,
        context: context,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Icon(
                  Icons.info_outline_rounded,
                  size: Re.widthRe(
                    context: context,
                    width: 8,
                  ),
                  color: const Color(0xffFFDC04),
                ),
                Text(
                  "Maximo de intentos fallidos",
                  style: TextStyle(
                    color: const Color(0xff353434),
                    fontSize: Re.adaptiveTextSize(
                      18,
                      context,
                    ),
                  ),
                ),
              ],
            ),
          ),
          _btnOk(),
        ],
      ),
    );
  }

  Widget _btnOk() {
    return Padding(
      padding: const EdgeInsets.only(right: 10.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          SizedBox(
            width: Re.widthRe(context: context, width: 30),
            height: 50,
            child: buttonInArea(
              context: context,
              buttonColor: Colors.white,
              text: "Aceptar",
              textSize: Re.adaptiveTextSize(
                10,
                context,
              ),
              onPressedFn: () {
                loginProvider.sendIsTwoFactors(false);
                Navigator.pop(context);
              },
              shadowColorButton: Colors.transparent,
              textColor: Colors.black,
              icon: const Icon(
                Icons.arrow_back,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
