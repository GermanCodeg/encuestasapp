// lib
import 'package:provider/provider.dart';

// flutter
import 'package:flutter/material.dart';

// utils
import 'package:app_encuestas/utils/responsive.dart';

// providers
import 'package:app_encuestas/providers/text_controller.dart';

class TextFieldPhoneContainer extends StatefulWidget {
  const TextFieldPhoneContainer({Key? key}) : super(key: key);

  @override
  State<TextFieldPhoneContainer> createState() =>
      _TextFieldPhoneContainerState();
}

class _TextFieldPhoneContainerState extends State<TextFieldPhoneContainer> {
  late TextControllerProvider textControllerProvider;
  @override
  Widget build(BuildContext context) {
    textControllerProvider = Provider.of<TextControllerProvider>(context);
    return textFieldPhoneContainer();
  }

  Widget textFieldPhoneContainer() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _labelTextField(),
            const SizedBox(
              height: 10,
            ),
            _textField(),
          ],
        ),
      ],
    );
  }

  Widget _labelTextField() {
    return Re.textRe(
      textWidth: Re.widthRe(
        context: context,
        width: 30,
      ),
      text: const Text(
        "Número celular",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _textField() {
    return Container(
      width: Re.widthRe(
        context: context,
        width: 70,
      ),
      height: Re.heightRe(
        context: context,
        heigh: 8,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(
            Re.widthRe(
              context: context,
              width: 2,
            ),
          ),
        ),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Re.textRe(
            textWidth: Re.widthRe(
              context: context,
              width: 7,
            ),
            text: const Text(
              "+51",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
              vertical: Re.heightRe(
                context: context,
                heigh: 2.0,
              ),
            ),
            child: VerticalDivider(
              color: Colors.black54,
              width: 1,
              thickness: Re.widthRe(
                context: context,
                width: 0.5,
              ),
            ),
          ),
          SizedBox(
            width: Re.widthRe(
              context: context,
              width: 40,
            ),
            child: TextField(
              controller: textControllerProvider.textPhoneController,
              autofocus: true,
              textAlignVertical: TextAlignVertical.center,
              style: TextStyle(
                fontSize: Re.widthRe(
                  context: context,
                  width: 5,
                ),
              ),
              decoration: const InputDecoration(
                  isCollapsed: true,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent),
                  ),
                  focusColor: Colors.transparent),
            ),
          ),
        ],
      ),
    );
  }
}
