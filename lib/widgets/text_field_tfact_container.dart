// lib
import 'package:provider/provider.dart';

// flutter
import 'package:flutter/material.dart';

// widgets
import 'package:app_encuestas/widgets/button_in_area.dart';

// utils
import 'package:app_encuestas/utils/responsive.dart';

// providers
import 'package:app_encuestas/providers/login_provider.dart';
import 'package:app_encuestas/providers/text_controller.dart';

class TextFieldTfactContainer extends StatefulWidget {
  const TextFieldTfactContainer({Key? key}) : super(key: key);

  @override
  State<TextFieldTfactContainer> createState() =>
      _TextFieldPhoneContainerState();
}

class _TextFieldPhoneContainerState extends State<TextFieldTfactContainer> {
  late TextControllerProvider textControllerProvider;
  late LoginProvider loginProvider;
  @override
  Widget build(BuildContext context) {
    textControllerProvider = Provider.of<TextControllerProvider>(context);
    loginProvider = Provider.of<LoginProvider>(context);
    return textFieldTwoFactorsContainer();
  }

  Widget textFieldTwoFactorsContainer() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            _labelTextField(),
            const SizedBox(
              height: 10,
            ),
            _textField(),
            _toBack(),
          ],
        ),
      ],
    );
  }

  Widget _labelTextField() {
    return Re.textRe(
      textWidth: Re.widthRe(
        context: context,
        width: 45,
      ),
      text: const Text(
        "Ingreso de dos factores",
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _textField() {
    return Container(
      width: Re.widthRe(
        context: context,
        width: 70,
      ),
      height: Re.heightRe(
        context: context,
        heigh: 8,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(
            Re.widthRe(
              context: context,
              width: 2,
            ),
          ),
        ),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            width: Re.widthRe(
              context: context,
              width: 70,
            ),
            child: TextField(
              autofocus: true,
              controller: textControllerProvider.textTfactController,
              textAlignVertical: TextAlignVertical.center,
              style: TextStyle(
                fontSize: Re.widthRe(
                  context: context,
                  width: 5,
                ),
              ),
              decoration: const InputDecoration(
                  contentPadding: EdgeInsets.symmetric(
                    horizontal: 15.0,
                  ),
                  isCollapsed: true,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent),
                  ),
                  focusColor: Colors.transparent),
            ),
          ),
        ],
      ),
    );
  }

  Widget _toBack() {
    return SizedBox(
      width: Re.widthRe(context: context, width: 70),
      child: Padding(
        padding: const EdgeInsets.only(right: 10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            SizedBox(
              width: Re.widthRe(context: context, width: 18),
              height: 50,
              child: buttonInArea(
                context: context,
                buttonColor: Colors.transparent,
                text: "Volver",
                textSize: Re.adaptiveTextSize(
                  15,
                  context,
                ),
                onPressedFn: () {
                  loginProvider.sendIsTwoFactors(false);
                },
                paddingButton: const EdgeInsets.symmetric(
                  horizontal: 0,
                  vertical: 0,
                ),
                shadowColorButton: Colors.transparent,
                textColor: Colors.white,
                icon: const Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
