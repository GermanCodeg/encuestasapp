// flutter
import 'package:flutter/material.dart';

// utils
import 'package:app_encuestas/utils/responsive.dart';

Widget buttonInArea({
  required BuildContext context,
  required Icon icon,
  required String text,
  required Color textColor,
  required Color buttonColor,
  double? btnWidth,
  double? textSize,
  EdgeInsetsGeometry? paddingButton,
  RoundedRectangleBorder? shapeButton,
  Color? shadowColorButton,
  VoidCallback? onPressedFn,
}) {
  EdgeInsetsGeometry? paddingBtn = paddingButton ??
      const EdgeInsets.symmetric(
        horizontal: 50,
        vertical: 15,
      );

  RoundedRectangleBorder? shapeBtn = shapeButton ??
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      );

  Color? shadowColorBtn = shadowColorButton ?? Colors.black;

  double? width = btnWidth ?? Re.widthRe(context: context, width: 19);

  return SizedBox(
    width: width,
    child: FittedBox(
      fit: BoxFit.fitWidth,
      child: ElevatedButton.icon(
        style: ElevatedButton.styleFrom(
          primary: buttonColor,
          padding: paddingBtn,
          shape: shapeBtn,
          shadowColor: shadowColorBtn,
        ),
        icon: icon,
        label: Text(
          text,
          style: TextStyle(color: textColor),
        ),
        onPressed: onPressedFn,
      ),
    ),
  );
}
